<?php include 'config.php';?>

<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Nome:',$_POST["name"],' ',
	'E-mail: ',$_POST["email"],' ',
	'Assunto: ',$_POST["subject"],' ',
	'Mensagem: ',$_POST["message"],' ',
	' ',
	'Contato realizado às: ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

$newFile = __DIR__.'../contato/'.$_POST["email"].".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($fields);

}
?>

<!DOCTYPE html>
<html>

<head>
    	<!-- meta character set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Impuls&atilde;o</title>		
		<!-- Meta Description -->
        <meta name="description" content="Nós te ajudamos a monetizar o seu Know-how de conhecimento!">
        <meta name="keywords" content="Know-how, monetizar, impulsionar, negócios">
        <meta name="author" content="Impulsão - Lançamentos digitais">
        <link rel="icon" type="image/png" href="img/icons/favicon.png" />
		
		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- CSS
		================================================== -->
		
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
		
		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/jquery.fancybox.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/owl.carousel.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/slit-slider.css">
		<!-- bootstrap.min -->
        <link rel="stylesheet" href="css/animate.css">
		<!-- Customized Stylesheet -->
        <link rel="stylesheet" href="css/style.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="css/main.css">


		<!-- Modernizer Script for old Browsers -->
        <script src="js/modernizr-2.6.2.min.js"></script>
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
      }
    </script>
    </head>
	
    <body id="body">

		<!-- preloader -->
		<div id="preloader">
            <div class="loder-box">
            	<div class="battery"></div>
            </div>
		</div>
		<!-- end preloader -->

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
                    </button>
					<!-- /responsive nav button -->
					
					<!-- logo -->
					<div class="navbar-brand">
						<a href="#home"><img src="img/logo/teste.png" > </a>
					</div>
					<!-- /logo -->
                </div>

				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#service">Servi&ccedil;os</a></li>
                        <li><a href="#portfolio">Clientes</a></li>
                        <li><a href="#contact">Contato</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->
				
            </div>
        </header>
        <!--
        End Fixed Navigation
        ==================================== -->
		
		<main class="site-content" role="main">
		
        <!--
        Home Slider
        ==================================== -->
		
		<section id="home-slider">
            <div id="slider" class="sl-slider-wrapper">

				<div class="sl-slider">
				
					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">

						<div class="bg-img bg-img-1"></div>

						<div class="slide-caption">
                            <div class="caption-content">
                                <h2 class="animated fadeInDown">Lan&ccedil;amentos digitais</h2>
                                <span class="animated fadeInDown">N&oacute;s te ajudamos a monetizar seu Know-how e conhecimento.</span>
                                <a href="#service" class="btn btn-blue btn-effect">Impulsione-se</a>
                            </div>
                        </div>
						
					</div>
					
				<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
					
						<div class="bg-img bg-img-2"></div>
						<div class="slide-caption">
                            <div class="caption-content">
                                <h2>Marketing Digital</h2>
                                <span>Voc&#234; vendendo muito mais pela internet!</span>
                                <a href="#service" class="btn btn-blue btn-effect">Impulsione-se</a>
                            </div>
                        </div>
						
					</div>
					
					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
						
						<div class="bg-img bg-img-3"></div>
						<div class="slide-caption">
                            <div class="caption-content">
                                <h2>Produ&#231;&#245;es Audiovisuais</h2>
                                <span>Engaje, venda e impacte muitas pessoas atrav&#233;s de v&#237;deos profissionais.</span>
                                <a href="#service" class="btn btn-blue btn-effect">Impulsione-se</a>
                            </div>
                        </div>

					</div>

				</div><!-- /sl-slider -->

                <!-- 
                <nav id="nav-arrows" class="nav-arrows">
                    <span class="nav-arrow-prev">Previous</span>
                    <span class="nav-arrow-next">Next</span>
                </nav>
                -->
                
                <nav id="nav-arrows" class="nav-arrows hidden-xs hidden-sm visible-md visible-lg">
                    <a href="javascript:;" class="sl-prev">
                        <i class="fa fa-angle-left fa-3x"></i>
                    </a>
                    <a href="javascript:;" class="sl-next">
                        <i class="fa fa-angle-right fa-3x"></i>
                    </a>
                </nav>
                

				<nav id="nav-dots" class="nav-dots visible-xs visible-sm hidden-md hidden-lg">
					<span class="nav-dot-current"></span>
					<span></span>
					<span></span>
				</nav>

			</div><!-- /slider-wrapper -->
		</section>
			
			
		<?php include 'service.php' ?>
		
		<!-- portfolio section -->
		<section id="portfolio">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center wow animated fadeInDown">
						<h2>CLIENTES</h2>
						
					</div>
					

<?php include 'clients.php' ?>
					
				</div>
			</div>
		</section>
		<!-- end portfolio section -->
		
		<!-- Testimonial section -->
		<!-- <section id="testimonials" class="parallax">
			<div class="overlay">
				<div class="container">
					<div class="row">
					
						<div class="sec-title text-center white wow animated fadeInDown">
							<h2>Testemunhos</h2>
						</div>
						
						<div id="testimonial" class=" wow animated fadeInUp">
						<?php include 'testmonies.php' ?>
							
						</div>
					
					</div>
				</div>
			</div>
		</section>-->
		<!-- end Testimonial section -->
		
		<!-- Price section -->
		<!--
		<section id="price">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center wow animated fadeInDown">
						<h2>Price</h2>
						<p>Our price for your company</p>
					</div>
					
					<div class="col-md-4 wow animated fadeInUp">
						<div class="price-table text-center">
							<span>Silver</span>
							<div class="value">
								<span>$</span>
								<span>24,35</span><br>
								<span>month</span>
							</div>
							<ul>
								<li>No Bonus Points</li>
								<li>No Bonus Points</li>
								<li>No Bonus Points</li>
								<li>No Bonus Points</li>
								<li><a href="#">sign up</a></li>
							</ul>
						</div>
					</div>
					
					<div class="col-md-4 wow animated fadeInUp" data-wow-delay="0.4s">
						<div class="price-table featured text-center">
							<span>Gold</span>
							<div class="value">
								<span>$</span>
								<span>50,00</span><br>
								<span>month</span>
							</div>
							<ul>
								<li>Free Trial</li>
								<li>Free Trial</li>
								<li>Free Trial</li>
								<li>Free Trial</li>
								<li><a href="#">sign up</a></li>
							</ul>
						</div>
					</div>
					
					<div class="col-md-4 wow animated fadeInUp" data-wow-delay="0.8s">
						<div class="price-table text-center">
							<span>Diamond</span>
							<div class="value">
								<span>$</span>
								<span>123,12</span><br>
								<span>month</span>
							</div>
							<ul>
								<li>All Bonus Points</li>
								<li>All Bonus Points</li>
								<li>All Bonus Points</li>
								<li>All Bonus Points</li>
								<li><a href="#">sign up</a></li>
							</ul>
						</div>
					</div>
	
				</div>
			</div>
		</section>-->
		<!-- end Price section -->
		
		<!-- Social section -->
		<!--<section id="social" class="parallax">
			<div class="overlay">
				<div class="container">
					<div class="row">
					
						<div class="sec-title text-center white wow animated fadeInDown">
							<h2>Siga-nos</h2>
							<p>Interaja com a Impulsão através das mídias sociais.</p>
						</div>
						
						<ul class="social-button">
							<li class="wow animated zoomIn"><a href="http://facebook.com.br"><i class="fa fa-thumbs-up fa-2x"></i></a></li>
							<li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
							<li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="#"><i class="fa fa-dribbble fa-2x"></i></a></li>							
						</ul>
						
					</div>
				</div>
			</div>
		</section>-->
		<!-- end Social section -->
		
		<!-- Contact section -->
		<section id="contact" >
			<div class="container">
				<div class="row">
					
					<div class="sec-title text-center wow animated fadeInDown">
						<h2>Contato</h2>
						
					</div>
					
					
					<div class="col-md-7 contact-form wow animated fadeInLeft">
						<form method="POST">
							<div class="input-field">
								<input type="text" name="name" class="form-control" placeholder="Seu nome" required="true">
							</div>
							<div class="input-field">
								<input type="email" name="email" class="form-control" placeholder="Seu e-mail">
							</div>
							<div class="input-field">
								<input type="text" name="subject" class="form-control" placeholder="Assunto">
							</div>
							<div class="input-field">
								<textarea name="message" class="form-control" placeholder="Mensagem"></textarea>
							</div>
					       	<button type="submit" id="submit" class="btn btn-blue btn-effect">Enviar</button>
						</form>
					</div>
					
					<div class="col-md-5 wow animated fadeInRight">
						<address class="contact-details">
							<h3>Entre em contato</h3>						
							<p><i class="fa fa-pencil"></i>Impuls&atilde;o - Lan&ccedil;amentos Digitais <span>Rua Acre, 10 - Estados</span> <span>Indaial, Santa Catarina </span><span>Brasil</span></p><br>
							<p><i class="fa fa-phone"></i>(47) 3333-4600 </p>
							<p><i class="fa fa-envelope"></i>contato@impulsao.com.br</p>
						</address>
					</div>
		
				</div>
			</div>
		</section>
		<!-- end Contact section -->
		
		</main>
		
		<footer id="footer">
			<div class="container">
				<div class="row text-center">
					<div class="footer-content">
						<div class="wow animated fadeInDown">
							<p>Cadastre-se em nossa newsletter</p>
							<p>Apenas assuntos importantes, nada de spam!</p>
						</div>


						<form action="https://leadlovers.com/Pages/Index/132682" method="post" >  
						   <input id="id" name="id" type="hidden" value="132682" />  
						   <input id="pid" name="pid" type="hidden" value="3796103" />  
						   <input id="list_id" name="list_id" type="hidden" value="132682" />  
						   <input id="provider" name="provider" type="hidden" value="leadlovers" />   
					
						   <input class="form-control" id="email" name="email" placeholder="Informe o seu email" type="text" />  
						   <button class="btn btn-danger" style="margin-top:10px;" type="submit">Cadastrar-se</button>  
						   <input type="hidden" id="source" name="source" value="" />  
						   <img src="https://llimages.com/redirect/redirect.aspx?A=V&p=3796103&m=132682" style="display: none;" />
						</form>
						<div class="footer-social">
							<ul>
								<li class="wow animated zoomIn"><a href="#"><i class="fa fa-thumbs-up fa-3x"></i></a></li>
								<li class="wow animated zoomIn" data-wow-delay="1.2s"><a href="#"><i class="fa fa-youtube fa-3x"></i></a></li>
							</ul>
						</div>
						
						<p>Copyright &copy; 2017 - Desenvolvido por Impulsão</p>
					</div>
				</div>
			</div>
		</footer>
		
		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
		<!-- Single Page Nav -->
        <script src="js/jquery.singlePageNav.min.js"></script>
		<!-- jquery.fancybox.pack -->
        <script src="js/jquery.fancybox.pack.js"></script>
		<!-- Google Map API -->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<!-- Owl Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- jquery easing -->
        <script src="js/jquery.easing.min.js"></script>
        <!-- Fullscreen slider -->
        <script src="js/jquery.slitslider.js"></script>
        <script src="js/jquery.ba-cond.min.js"></script>
		<!-- onscroll animation -->
        <script src="js/wow.min.js"></script>
		<!-- Custom Functions -->
        <script src="js/main.js"></script>
    </body>


</html>
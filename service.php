<!-- Service section -->
		<section id="service">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center">
						<h2 class="wow animated bounceInLeft">Servi&ccedil;os</h2>
						
					</div>
					
					<div class="col-md-4 col-sm-6 col-xs-12 text-center wow animated zoomIn">
						<div class="service-item">
							<div class="service-icon">
								<i class="fa fa-rocket fa-3x"></i>
							</div>
							<h3>Lan&ccedil;amentos digitais</h3>
							<p>Imagine quantas pessoas pagariam por seu conhecimento e experi&#234;ncia. N&#243;s te ajudamos a chegar at&#233; estas pessoas e dar escala ao seu conhecimento. Somos especialistas em lan&#231;ar pessoas que tem muito conhecimento, mas que n&#227;o sabem como monetiz&#225;-lo!</p>
						</div>
					</div>
				
					<div class="col-md-4 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.3s">
						<div class="service-item">
							<div class="service-icon">
								<i class="fa fa-facebook fa-3x"></i>
							</div>
							<h3>Marketing Digital</h3>
							<p>Sua empresa posicionada de forma estrat&#233;gica na internet, vendendo muito mais e impactando as pessoas certas. N&#243;s desenvolvemos e aplicamos as melhores estrat&#233;gias de marketing digital para voc&#234; e sua empresa! </p>
						</div>
					</div>
				
					<div class="col-md-4 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.6s">
						<div class="service-item">
							<div class="service-icon">
								<i class="fa fa-file-video-o fa-3x"></i>
							</div>
							<h3>Produ&#231;&#245;es Audiovisuais</h3>
							<p>O apelo audiovisual &#233; cada vez maior na internet. V&#237;deos para marcas e lan&#231;amentos, n&#227;o s&#227;o tend&#234;ncia, s&#227;o realidade e necessidade. N&#243;s somos a empresa que vai te impulsionar atrav&#233;s de produ&#231;&#245;es audiovisuais com qualidade de cinema!</p>
						</div>
					</div>
				
					
					
				</div>
			</div>
		</section>
		<!-- end Service section -->
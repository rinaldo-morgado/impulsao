					
					<ul class="project-wrapper wow animated fadeInUp">
						<li class="portfolio-item">
							<img src="img/portfolio/dr_planeja.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>Dr. Planeja</h3>
								<p></p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						
						<li class="portfolio-item">
							<img src="img/portfolio/lulis.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>Lulis</h3>
								<p</p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						
						<li class="portfolio-item">
							<img src="img/portfolio/oxi3.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>Oxi3</h3>
								<p></p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						

					</ul>


					<ul class="project-wrapper wow animated fadeInUp">
						<li class="portfolio-item">
							<img src="img/portfolio/modaboho.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>Moda Boho</h3>
								<p></p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						
						<li class="portfolio-item">
							<img src="img/portfolio/fazendo-moda.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>Fazendo Moda</h3>
								<p></p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						
						<li class="portfolio-item">
							<img src="img/portfolio/psicovendas.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>PsicoVendas</h3>
								<p></p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						

					</ul>


					<ul class="project-wrapper wow animated fadeInUp">
						<li class="portfolio-item">
							<img src="img/portfolio/gestao-de-valores.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>Gestão de Valores</h3>
								<p></p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						
						<li class="portfolio-item">
							<img src="img/portfolio/juci-nones.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>45 Dias com Juci Nones</h3>
								<p></p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						
						<li class="portfolio-item">
							<img src="img/portfolio/purificar.png" class="img-responsive" alt="Referência em liberdade, estilo e elegância para as mulheres. Misturando estilos, cores, formas e sensações.">
							<figcaption class="mask">
								<h3>Purificar do Brasil</h3>
								<p></p>
							</figcaption>
							<ul class="external">

								<!--<li><a href="http://lulis.com.br" target="_blank"><i class="fa fa-link"></i></a></li>-->
							</ul>
						</li>
						

					</ul>